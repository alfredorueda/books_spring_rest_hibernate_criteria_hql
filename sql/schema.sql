CREATE TABLE books
(
  id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  author VARCHAR(255),
  isbn VARCHAR(255),
  numSells INT,
  publishedDate DATETIME,
  title VARCHAR(255)
);
