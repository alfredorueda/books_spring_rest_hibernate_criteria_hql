INSERT INTO bookdatabase.books (id, author, isbn, numSells, publishedDate, title) VALUES (1, 'Dumbledore ', '123456789', 100000, '2015-03-20 13:22:28', 'Spring Spells');
INSERT INTO bookdatabase.books (id, author, isbn, numSells, publishedDate, title) VALUES (2, 'Voldermort', '123456789', 900000, '2015-03-20 13:22:28', 'Advanced Spring Spells');
INSERT INTO bookdatabase.books (id, author, isbn, numSells, publishedDate, title) VALUES (3, 'Harry Potter', '123456789', 900000, '2015-03-20 13:22:28', 'Advanced Spring Spells 2');
