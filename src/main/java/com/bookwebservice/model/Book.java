package com.bookwebservice.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "books")
public class Book implements Comparable<Book> {

	public Book(){}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "isbn")
	private String isbn;

	@Column(name = "author")
	private String author;

	@Column(name = "title")
	private String title;

	@Column(name = "numSells")
	private int numSells;

	@Column(name = "publishedDate")
	private Date publishedDate;


	public Book(long id, String isbn, String title, String author, int numSells,Date publishedDate) {
		super();
		this.id = id;
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.numSells = numSells;
		this.publishedDate=publishedDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getNumSells() {
		return numSells;
	}

	public void setNumSells(int numSells) {
		this.numSells = numSells;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	@Override
	public String toString() {
		return "\n Book [id="
				+ id
				+ ", "
				+ (isbn != null ? "isbn=" + isbn + ", " : "")
				+ (author != null ? "author=" + author + ", " : "")
				+ (title != null ? "title=" + title + ", " : "")
				+ "numSells="
				+ numSells
				+ ", "
				+ (publishedDate != null ? "publishedDate=" + publishedDate
						: "") + "]";
	}

	@Override
	public int compareTo(Book o) {
		//ASC
		return o.getNumSells() - this.getNumSells();
	}
}