package com.bookwebservice.utils;

import com.bookwebservice.model.Book;

import java.util.Comparator;

//Clase que compara los libros
public class CompareBookBySalesDesc implements Comparator<Book>{

	@Override
	public int compare(Book b1, Book b2) {
		
		return b2.getNumSells()-b1.getNumSells() ;
	}

}
