package com.bookwebservice.dao;

import com.bookwebservice.model.Book;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {

	@Override
	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Book(rs.getLong("id"), rs.getString("isbn"),
				rs.getString("title"), rs.getString("author"),
				rs.getInt("numSells"),new java.util.Date(rs.getDate("publishedDate").getTime()) );
	}
}
