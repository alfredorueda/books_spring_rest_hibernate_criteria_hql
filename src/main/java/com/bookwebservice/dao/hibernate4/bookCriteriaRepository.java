package com.bookwebservice.dao.hibernate4;

import com.bookwebservice.dao.BookDAO;
import com.bookwebservice.model.Book;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by JhanCarlos on 23/12/14.
 */
@Repository("bookDAOHibernateCriteria")
@Transactional
public class bookCriteriaRepository implements BookDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Book> getBooks(String[] queryParamsArray, Object[] valuesArray) {

        Criteria criteria = currentSession().createCriteria(Book.class);

        int i = 0;
        for(String param: queryParamsArray){

            Object value = valuesArray[i];

            if(value instanceof String){
                criteria.add(Restrictions.like(param, "%" + value + "%"));
            }else{
                criteria.add(Restrictions.eq(param, value));
            }
            i++;
        }

        List<Book> books = criteria.list();

        return books;
    }

    @Override
    public List<Book> getTopSellsBooks(int topsell) {

        Criteria criteria = currentSession().createCriteria(Book.class);

        criteria.setMaxResults(topsell);

        criteria.addOrder(Order.desc("numSells"));

        return criteria.list();
    }

    @Override
    public List<Book> getByRangeOfDate(Date minDate, Date maxDate) {

        Criteria criteria = currentSession().createCriteria(Book.class);

        criteria.add(Restrictions.between("publishedDate", minDate, maxDate));

        return criteria.list();
    }

    @Override
    public Book create(Book book) {
	    currentSession().save(book);

		return book;
    }

    @Override
    public Book getBook(Long id) {
        return (Book) currentSession().get(Book.class,id);
    }

    @Override
    public Book updateBook(Book book) {

        currentSession().saveOrUpdate(book);
	    return book;
    }

    @Override
    public void deleteBook(Long id) {
        currentSession().delete(getBook(id));
    }

    @Override
    public List<Book> getBooksByAuthor(String author) {

            Criteria criteria = currentSession().createCriteria(Book.class);

            criteria.add(Restrictions.like("author", "%" + author + "%"));

            return criteria.list();

    }
}
