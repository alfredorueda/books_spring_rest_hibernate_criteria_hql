package com.bookwebservice.dao.jdbc;

import com.bookwebservice.dao.BookDAO;
import com.bookwebservice.dao.BookMapper;
import com.bookwebservice.model.Book;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Repository("bookDAOjdbcTemplate")
public class BookJDBCTemplate implements BookDAO {

	@Resource
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Book> getBooks(String[] queryParamsArray, Object[] valuesArray) {
		// TODO Auto-generated method stub


		String query = "SELECT * FROM books WHERE ";

		if(0 == valuesArray.length){
			query = "SELECT * FROM books;";
		}

		for (int i = 0; i < queryParamsArray.length; i++) {

			if (Objects.equals(queryParamsArray[i], "id")) {
				queryParamsArray[i] = "id = ?";
			}

			if (Objects.equals(queryParamsArray[i], "isbn")) {
				queryParamsArray[i] = "isbn = ?";
			}

			if (Objects.equals(queryParamsArray[i], "title")) {
				queryParamsArray[i] = "title LIKE ?";
				valuesArray[i] = "%" + valuesArray[i] + "%";
			}

			if (Objects.equals(queryParamsArray[i], "author")) {
				queryParamsArray[i] = "author LIKE ?";
				valuesArray[i] = "%" + valuesArray[i] + "%";
			}

			if (Objects.equals(queryParamsArray[i], "numSells")) {
				queryParamsArray[i] = "numSells = ?";
			}

			if (Objects.equals(queryParamsArray[i], "publishedDate")) {
				queryParamsArray[i] = "publishedDate = ?";
			}

		}

		for (int i = 0; i < queryParamsArray.length; i++) {
			if (i != 0) query += " AND ";
			query += queryParamsArray[i];

		}

		//Devuelve una lista de libros que pueden ser buscados pasandole cualquier parametro del la clase book
		return jdbcTemplate.query(query,valuesArray, new BookMapper());

	}

	@Override
	public List<Book> getTopSellsBooks(int topsell) {
		//devuelve una lista de libros utilizando el parametro numSells ordenados de desc por el numero de ventas
		return jdbcTemplate.query("Select * from books ORDER BY numSells DESC LIMIT "+topsell+" ;",new BookMapper());

	}

	@Override
	public List<Book> getByRangeOfDate(Date minDate, Date maxDate) {
		Object[] values = new Object[]{new java.sql.Date(minDate.getTime()),new java.sql.Date(maxDate.getTime())};
		//Devuelve una lista de libros pasandole una rango de fechas
		return jdbcTemplate.query("Select * from books WHERE publishedDate BETWEEN ? AND ? ;",values,new BookMapper());

	}

	@Override
	public Book getBook(Long id) {
		//devielve un libro pasandole la id
		return jdbcTemplate.queryForObject("SELECT * FROM books WHERE id ="+ id, new BookMapper());
	}

	@Override
	public Book create(Book book) {
		//Inserta un libro en la base de datos
		jdbcTemplate.update("INSERT INTO books (isbn,title,author,numSells,publishedDate) values(?,?,?,?,?)",
		book.getIsbn(), book.getTitle(), book.getAuthor(),book.getNumSells(), book.getPublishedDate());

		//TODO Pending return Book.
		return null;
	}

	@Override
	public Book updateBook(Book book) {
		//Actualiza un libro
		java.sql.Date publishedDate = new java.sql.Date(book.getPublishedDate().getTime());

		jdbcTemplate.update("UPDATE books SET"
				+ " isbn = '" + book.getIsbn()+ "',"
				+ " title = '" + book.getTitle() + "',"
				+ " author = '"+ book.getAuthor() + "',"
				+ " numSells = '" + book.getNumSells()+ "',"
				+ " PublishedDate = '" + publishedDate + "'"
				+ " WHERE id = " + book.getId());

		return book;
	}

	@Override
	public void deleteBook(Long id) {
		// TODO Auto-generated method stub
		//Borra un libro
		jdbcTemplate.update("DELETE FROM books WHERE id = " + id);
	}

	@Override
	public List<Book> getBooksByAuthor(String author) {
		Object[] values = new Object[]{"%"+author+"%"};

		return jdbcTemplate.query("SELECT * FROM books WHERE author LIKE ?",values,new BookMapper());
	}


}
