package com.bookwebservice.dao;

import com.bookwebservice.model.Book;

import java.util.Date;
import java.util.List;

public interface BookDAO {

	public List<Book> getBooks(String[] queryParams, Object[] values);

	public List<Book> getTopSellsBooks(int topsell);
	
	public List<Book> getByRangeOfDate(Date minDate, Date maxDate);

	public Book create(Book book);

	public Book getBook(Long id);
	
	public Book updateBook(Book book);
	
	public void deleteBook(Long id);

	List<Book> getBooksByAuthor(String author);
}
