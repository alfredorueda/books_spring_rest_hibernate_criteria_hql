package com.bookwebservice.dao.hql;

import com.bookwebservice.dao.BookDAO;
import com.bookwebservice.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by DAM on 27/1/15.
 */
@Repository("bookDAHibernateHql")
@Transactional
public class bookHqlRepository implements BookDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }


    @Override
    public List<Book> getBooks(String[] queryParams, Object[] values) {

        String wherePart = "WHERE ";

        for(int i = 0; i < queryParams.length; i++) {

            if(values[i] == null) continue;

            if(i != 0) {
                wherePart += " AND ";
            }

            if(values[i] instanceof String) {
                wherePart += "books." + queryParams[i] + " LIKE '%" + values[i] + "%'";
            }
            else if(queryParams[i].equals("minDate")) {
                java.sql.Date minDate = new java.sql.Date(
                        ((java.util.Date)values[i]).getTime());
                wherePart += "books.publishedDate" + ">='" + minDate + "'";
            }
            else if(queryParams[i].equals("maxDate")) {
                java.sql.Date maxDate = new java.sql.Date(
                        ((java.util.Date)values[i]).getTime());
                wherePart += "books.publishedDate" + "<='" + maxDate + "'";
            }
            else wherePart += queryParams[i] + "=" + values[i];
        }

        String hqlQuery = "from Book as books " + wherePart;

        if(values.length == 0){
            hqlQuery = "from Book as books";
        }

        return (List<Book>) currentSession().createQuery(hqlQuery).list();
    }

    @Override
    public List<Book> getTopSellsBooks(int topsell) {

        String hqlQuery = "from Book as books ORDER BY books.numSells DESC";

        List <Book> books = (List<Book>) currentSession()
                .createQuery(hqlQuery)
                .setMaxResults(topsell)
                .list();

        return books;
    }

    @Override
    public List<Book> getByRangeOfDate(Date minDate, Date maxDate) {

        String hqlQuery = "from Book as books where books.publishedDate BETWEEN :minDate AND :maxDate";

        List <Book> books = (List<Book>) currentSession()
                .createQuery(hqlQuery)
                .setParameter("minDate",minDate)
                .setParameter("maxDate",maxDate)
                .list();

        return books;

    }

    @Override
    public Book create(Book book) {

        currentSession().save(book);

	    return book;
    }

    @Override
    public Book getBook(Long id) {
        String hqlQuery = "from Book as books where books.id = " + id;

        Book book = (Book) currentSession().createQuery(hqlQuery).uniqueResult();

        return book;
    }

    @Override
    public Book updateBook(Book book) {
        currentSession().update(book);
	    return book;
    }

    @Override
    public void deleteBook(Long id) {

        currentSession().delete(getBook(id));
    }

    @Override
    public List<Book> getBooksByAuthor(String author) {

        String hqlQuery = "from Book as books where books.author LIKE :author ";

        List <Book> books = (List<Book>) currentSession()
                .createQuery(hqlQuery)
                .setString("author","%"+author+"%")
                .list();

        return books;

    }
}
