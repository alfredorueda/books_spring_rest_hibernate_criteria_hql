package com.bookwebservice.controller;

import com.bookwebservice.model.Book;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Date;


public class BookValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Book.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		Book book = (Book) target;
		
		String isbn = book.getIsbn();
		String author = book.getAuthor();
		String title = book.getTitle();
		Date publishedDate = book.getPublishedDate();
		
		if(!NumberUtils.isNumber(isbn)){
			
			errors.rejectValue("isbn", "error.isbn.invalid", "Introduce un isbn correcto");
		} else if (isbn.length() != 10 || isbn.length() != 13){
			
			errors.rejectValue("isbn", "error.isbn.less", "Debe ser de 10 o 13 digitos");
		}
		
		if(author == null || "".equalsIgnoreCase(author)){
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "author", "error.author.blank", "No puede estar vacio");
		}
		
		if(title == null || "".equalsIgnoreCase(title)){
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.title.blank", "No puede estar vacio");
		}
		
		if(publishedDate == null){
			
			ValidationUtils.rejectIfEmpty(errors, "publishedDate", "error.publishedDate.blank", "No puede estar sin definir");
		}
	}

}
