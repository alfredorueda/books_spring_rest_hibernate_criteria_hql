package com.bookwebservice.controller;

import com.bookwebservice.dao.BookDAO;
import com.bookwebservice.model.Book;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//Para testing: http://localhost:8080/books?title=Spring Spells

// git remote add origin https://alfredorueda@bitbucket.org/alfredorueda/books_spring_rest_hibernate_criteria_hql.git



@RestController
public class BookController {
	//Con la anotación @Resource inyectamos la implementación de la interfaz BookDAO.
	// De este modo es muy sencillo cambiar la implementación de la capa de persistencia.

	//@Resource(name = "bookDAHibernateHql")
	@Resource(name = "bookDAOHibernateCriteria")
	BookDAO bookDAO;


	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public List<Book> books(
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "author", required = false) String author,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "isbn", required = false) String isbn,
			@RequestParam(value = "numSells", required = false) Integer numSells,
			@RequestParam(value = "publishedDate", required = false) Long publishedDateLong) {

		ArrayList <Object> values = new ArrayList<>();
		ArrayList <String> querysParams = new ArrayList<>();

		if (id != null) {
			querysParams.add("id");
			values.add(id);
			
		}
		if (isbn != null) {
			querysParams.add("isbn");
			values.add(isbn);
			
		}
		if (title != null) {
			querysParams.add("title");
			values.add(title);
		}
		if (author != null) {
			querysParams.add("author");
			values.add(author);
		}
		if (numSells != null) {
			querysParams.add("numSells");
			values.add(numSells);
			
		}
		if (publishedDateLong != null) {
			Date publishedDate = new Date(publishedDateLong);
			querysParams.add("publishedDate");
			values.add(publishedDate);
		}

		//Contiene los parametros que vamos a consultar
		String[] queryParamsArray = querysParams.toArray(new String[querysParams.size()]);
		//Contiene los valores de la consulta
		Object[] valuesArray = values.toArray(new Object[values.size()]);
		//Lista con los resultados de la consulta

		return bookDAO.getBooks(queryParamsArray, valuesArray);
	}
	//Peticiones de x libros ordenados por numero de ventas, default 10 libros
	@RequestMapping(value = "/topsells", method = RequestMethod.GET)
	public List<Book> topsells(
			@RequestParam(value = "topsell",defaultValue = "10",required = true) int topsell){

		System.out.println("");
		return bookDAO.getTopSellsBooks(topsell);
	}
	//Peticiones de libros por rango de fechas
	@RequestMapping(value = "/booksbydaterange", method = RequestMethod.GET)
	public List<Book> booksbydaterange(
			@RequestParam(value = "mindate", required = true) Long minDateLong,
			@RequestParam(value = "maxdate", required = true) Long maxDateLong){

		Date minDate = null;
		Date maxDate =  null;

		if (minDateLong != null) {
			minDate = new Date(minDateLong);
		}
		if (maxDateLong != null) {
			maxDate = new Date(maxDateLong);
		}

		return bookDAO.getByRangeOfDate(minDate, maxDate);
	}
	//Insertar un libro en la base de datos
	@RequestMapping(value = "/books", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Book newBook(@RequestBody Book book ){

		book = bookDAO.create(book);

		return book;

	}
	//Actualizar un libro
	@RequestMapping(value = "/books", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Book editBook(
			@RequestBody Book book ){

		bookDAO.updateBook(book);

		return book;
	}
	
	@RequestMapping(value = "/books", method = RequestMethod.DELETE)
	public void deleteBook(
			@RequestParam(value = "id", required = true) Long id){
		
		bookDAO.deleteBook(id);

	}

	@RequestMapping( value ="/books", method = RequestMethod.GET, params = "author")
	public List<Book> getBooksByAuthor(
			@RequestParam("author") String author) {

		return bookDAO.getBooksByAuthor(author);
	}
}